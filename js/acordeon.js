$(document).ready(function(){
	$(".list-group-item-text").hide();
	$(".list-group-item-heading").click(function(){
		$(this).next().toggle(500);
		$(this).parent().toggleClass("active");
		if($(this).parent().siblings().hasClass("active")){
			$(this).parent().siblings().removeClass("active");
		}

	});
});