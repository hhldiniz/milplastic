function randomizaCor(objeto)
{
	switch(Math.floor(Math.random()*7))
	{
		case 1:
			objeto.css("background-color","#fcf53a");
			break;
		case 2:
			objeto.css("background-color","#fa7921");
			break;
		case 3:
			objeto.css("background-color","#f94f4f");
			break;
		case 4:
			objeto.css("background-color","#ca2cea");
			break;
		case 5:
			objeto.css("background-color","#4b8dea");
			break;
		case 6:
			objeto.css("background-color","#9bc53d");
			break;
		default:
			randomizaCor(objeto);
	}
}
$(document).ready(function(){
	$(".encarte, .page-header, .list-group, #texto_historia, #foto_mil, #slogan, #contato, #lista_todos_produtos").hide();
	randomizaCor($("#home>a"));
	$("#home").click(function(){
		$(this).addClass("active");
		$(this).siblings().removeClass("active");
		$(this).siblings().children().css("background-color","#ffffff");
		randomizaCor($(this).children());
		$("#produtos_destaques, #carousel-home, #novidades").show();
		$("#texto_historia, #foto_mil, .encarte, #slogan, .page-header, .list-group, #contato, #lista_todos_produtos").hide();
	});
	$("#historia").click(function(){
		$(this).addClass("active");
		$(this).siblings().removeClass("active");
		$(this).siblings().children().css("background-color","#ffffff");
		$("#slogan, #foto_mil, #texto_historia").show();
		$("#carousel-home, #produtos_destaques, #contato, #novidades, #lista_todos_produtos").hide();
		randomizaCor($(this).children());
	});
	$("#logo").click(function(){
		$("#historia").addClass("active");
		randomizaCor($(("#home>a")));
	});
	$("#todos_produtos").click(function(){
		$(this).addClass("active");
		$(this).siblings().removeClass("active");
		$(this).siblings().children().css("background-color","#ffffff");
		randomizaCor($(this).children());
		$("#produtos_destaques, #carousel-home, #novidades, #texto_historia, #foto_mil, .encarte, #slogan, .page-header, .list-group, #contato").hide();
		$("#lista_todos_produtos").show();
	});
	$("#catalogo").click(function(){
		$(this).addClass("active");
		$(this).siblings().removeClass("active");
		$(this).siblings().children().css("background-color","#ffffff");
		randomizaCor($(this).children());
		$(".encarte, .page-header").show();
		$("#texto_historia, #foto_mil, #slogan, .list-group, #carousel-home, #produtos_destaques, #contato, #novidades, #lista_todos_produtos").hide();
	});
	$("#representantes").click(function(){
		$(this).addClass("active");
		$(this).siblings().removeClass("active");
		$(this).siblings().children().css("background-color","#ffffff");
		randomizaCor($(this).children());
		$(".list-group").show();
		$("#texto_historia, #foto_mil, .encarte, #slogan, .page-header, #carousel-home, #produtos_destaques, #contatom, #novidades, #lista_todos_produtos").hide();
	});
	$("#contatos").click(function(){
		$(this).addClass("active");
		$(this).siblings().removeClass("active");
		$(this).siblings().children().css("background-color","#ffffff");
		randomizaCor($(this).children());
		$("#contato").show();
		$("#texto_historia, #foto_mil, .encarte, #slogan, .page-header, #carousel-home, .list-group, #produtos_destaques, #novidades, #lista_todos_produtos").hide();
	});
	$("#email").click(function(){
		$(this).addClass("active");
		$(this).siblings().removeClass("active");
		$(this).siblings().children().css("background-color","#ffffff");
		randomizaCor($(this).children());
		$("#foto_mil, .encarte, #slogan, .page-header, #carousel-home, .list-group, #produtos_destaques, #contato, #novidades, #lista_todos_produtos").hide();
	});
	$("footer").css("margin-top",$(window).height()*0.7);
});